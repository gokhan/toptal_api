# README

* Call to authenticate and receive jwt token
User
curl -H "Content-Type: application/json" -X POST \
     -d '{"auth": {"email": "user@example.com", "password": "user"}}' \
     http://localhost:3000/api/authenticate

Manager
curl -H "Content-Type: application/json" -X POST \
     -d '{"auth": {"email": "manager@example.com", "password": "manager"}}' \
     http://localhost:3000/api/authenticate

Admin
curl -H "Content-Type: application/json" -X POST \
     -d '{"auth": {"email": "admin@example.com", "password": "admin"}}' \
     http://localhost:3000/api/authenticate

* Update user
curl -H "Content-Type:application/json" \
     -H "Authorization: Bearer ${TOKEN}" \
     -X PATCH -d '{"user": {"email": "user2@example.com", "password": ""}}' \
     http://localhost:3000/api/users/5

* Delete a user
curl -H "Content-Type:application/json" \
    -H "Authorization: Bearer ${TOKEN}" \
    -X DELETE  \
    http://localhost:3000/api/users/28

* List user (user manager or admin)
curl -H "Authorization: Bearer ${TOKEN}" \
    -X GET http://localhost:3000/api/users

* Update timezone
curl -H "Content-Type:application/json" \
     -H "Authorization: Bearer ${TOKEN}" \
     -X PATCH -d '{"timezone": {"name": "Updated", "city":"Chiang Mai", "difference":"7.0"}}' \
     http://localhost:3000/api/timezones/4

* Delete timezone
curl -H "Content-Type:application/json" \
    -H "Authorization: Bearer ${TOKEN}" \
    -X DELETE  \
    http://localhost:3000/api/timezones/28

* Search timezone
curl -H "Content-Type:application/json" \
     -H "Authorization: Bearer ${TOKEN}" \
     -X GET -d '{"keyword": "First"}' \
     http://localhost:3000/api/timezones/search
# TODO

- User CRUD operations (including to edit email and password)
- Timezones CRUD operations
