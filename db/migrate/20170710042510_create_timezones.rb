class CreateTimezones < ActiveRecord::Migration[5.1]
  def change
    create_table :timezones do |t|
      t.references :user, foreign_key: true
      t.string :name, null: false
      t.string :city, null: false
      t.decimal :difference, null: false, precision: 3, scale: 1

      t.timestamps
    end
  end
end
