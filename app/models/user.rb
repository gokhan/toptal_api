class User < ApplicationRecord
  has_secure_password
  has_many :timezones, dependent: :delete_all
end
