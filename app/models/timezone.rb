class Timezone < ApplicationRecord
  belongs_to :user

  include PgSearch  # full text search
  pg_search_scope :search_for, against: :name,
                  :using => {:tsearch => {:prefix => true, :dictionary => "english"} }
end
