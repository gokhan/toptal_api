class Timezone::Search < Trailblazer::Operation
  step :list!

  def list!(options, current_user:, params:, **)
    options["model"] = timezones(current_user).search_for(params[:keyword]).order(:id)
  end

  def timezones(user)
    if user.role == 'admin'
      Timezone.all
    else
      user.timezones
    end
  end
end
