require 'reform/form/dry'

class Timezone::Update < Trailblazer::Operation
	extend Contract::DSL
	contract do
		feature Reform::Form::Dry

		property :name
		property :city
		property :difference

		validation :default do
			required(:name).filled
			required(:city).filled
			required(:difference).value(type?: Float)
		end
	end
	step	Model( Timezone, :find_by )
	step  Policy::Pundit( Timezone::Policy, :update? )
  step 	Contract::Build()
  step  Contract::Validate(key: :timezone)
  step	Contract::Persist()
end
