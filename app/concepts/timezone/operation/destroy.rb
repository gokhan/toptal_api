class Timezone::Destroy < Trailblazer::Operation
  step Model( Timezone, :find_by)
  step Policy::Pundit( Timezone::Policy, :destroy? )
  step :destroy!

  def destroy!(options, **)
    options["model"].destroy
  end
end
