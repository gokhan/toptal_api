require 'reform/form/dry'

class Timezone::Create < Trailblazer::Operation
	extend Contract::DSL
	contract do
		feature Reform::Form::Dry

		property :name
		property :city
		property :difference

		validation :default do
			required(:name).filled
			required(:city).filled
			required(:difference).filled
		end
	end
	step	Model( Timezone, :new )
	step  :assign_user
  step 	Contract::Build()
  step  Contract::Validate(key: "timezone")
  step	Contract::Persist()

	def assign_user(options, current_user:, **)
		options["model"].user = current_user
	end
end
