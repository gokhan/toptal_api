class Timezone::Index < Trailblazer::Operation
  step :list!

  def list!(options, current_user:, **)
    options["model"] = timezones(current_user).order(:id)
  end

  def timezones(user)
    if user.role == 'admin'
      Timezone.all
    else
      user.timezones
    end
  end
end
