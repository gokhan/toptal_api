class Timezone::Policy < ApplicationPolicy
  def index?
    @user.present?
  end

  def update?
    owner? or admin?
  end

  def destroy?
    update?
  end
end
