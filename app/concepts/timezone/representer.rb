require 'representable/json'

class TimezoneRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :city
  property :difference
  property :current, exec_context: :decorator
  
  def current
    Time.now.to_s(:short)
    local = DateTime.now
    local.new_offset(Rational(represented.difference,24)).to_s(:short)
  end
end
