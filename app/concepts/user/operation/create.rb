require 'reform/form/dry'

class User::Create < Trailblazer::Operation
	extend Contract::DSL
	contract do
		feature Reform::Form::Dry
		property :email
		property :password

		validation :default do
			configure do
				config.messages_file = 'config/locales/error_messages.yml'

		     def email?(value)
		       (value =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i)
		     end

				 def unique?(value)
				 	 User.find_by(email: value).nil?
				 end
		   end

			required(:email).filled(:email?, :unique?)
			required(:password).filled
		end
	end
	step	Model( ::User, :new )
  step 	Contract::Build()
  step  Contract::Validate(key: "user")
  step	Contract::Persist()
end
