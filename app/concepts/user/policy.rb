class User::Policy < ApplicationPolicy
  def index?
    admin? or manager?
  end

  def update?
    index? or self?
  end

  def destroy?
    index?
  end

  private
  def self?
    @user == @record
  end
end
