class ApplicationController < ActionController::API
  include Knock::Authenticable

  private
  # Inject current_user into TB operations
  def _run_options(options)
    options.merge( "current_user" => current_user )
  end

  def unauthorized_entity(entity)
    render json: { error: 'Unauthorized access' }, status: 401
  end

  def breach_or_form_message(result, form)
    if result["result.policy.default"].present? and result["result.policy.default"]["message"].present?
      return result["result.policy.default"]["message"]
    elsif form.present? and form.errors.present? and !form.errors.empty?
      form.errors.messages
    else
      "Unknown problem"
    end
  end
end
