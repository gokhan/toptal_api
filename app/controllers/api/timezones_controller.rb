class Api::TimezonesController < ApplicationController
  before_action :authenticate_user

  def index
    run Timezone::Index do |result|
      return render( json: TimezoneRepresenter.for_collection.new(result["model"]) )
    end

    render json: {error: 'Unauthorized access'}, status: 422
  end

  def search
    run Timezone::Search do |result|
      return render( json: TimezoneRepresenter.for_collection.new(result["model"]) )
    end

    render json: {error: 'Unauthorized access'}, status: 422
  end

  def create
    run Timezone::Create do |result|
      return render(json: result["model"])
    end

    render json: {error: @form.errors.messages}
  end

  def update
    run Timezone::Update do |result|
      return render(json: result["model"])
    end

    render json: {error: breach_or_form_message(result, @form)}
  end

  def destroy
    run Timezone::Destroy do |result|
      return render(json: {})
    end

    render json: {}
  end
end
