Fabricator(:user) do
  email     "user@example.com"
  password  "user"
  role      "user"
end

Fabricator(:manager, from: :user) do
  email     "manager@example.com"
  password  "manager"
  role      "manager"
end


Fabricator(:admin, from: :user) do
  email     "admin@example.com"
  password  "admin"
  role      "admin"
end
