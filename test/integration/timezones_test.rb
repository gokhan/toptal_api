require 'test_helper'

class TimezonesTest < ActionDispatch::IntegrationTest
  include TokenHelpers

  # creation
  def test_create_successfully

    post api_timezones_url, params: {"timezone" => {"name" => "Chiang Mai", "city" => "Chiang Mai", "difference" => '7' }},
                            headers: header_for(:user)


    timezone = Timezone.first
    assert_nil json_response["error"]
    assert_equal "Chiang Mai", timezone.name
    assert_equal 7.0, timezone.difference
    assert_response :success
  end


  # update
  def test_update_successfully_by_user

    user = Fabricate(:user)
    timezone = Fabricate(:timezone, user: user)

    patch api_timezone_url(timezone), params: { "timezone" => {"name" => "Sweet Home", "difference" => '2'} },
                            headers: header_for(:user, user: user)

    timezone.reload
    assert_equal "Amsterdam", timezone.city
    assert_equal "Sweet Home", timezone.name
    assert_equal 2.0, timezone.difference

    assert_response :success
  end

  def test_update_successfully_by_admin

    user = Fabricate(:user)
    timezone = Fabricate(:timezone, user: user)

    patch api_timezone_url(timezone), params: { "timezone" => {"name" => "Sweet Home", "difference" => '2'} },
                            headers: header_for(:admin)

    timezone.reload
    assert_equal "Amsterdam", timezone.city
    assert_equal "Sweet Home", timezone.name
    assert_equal 2.0, timezone.difference

    assert_response :success
  end

  def test_update_fails_by_manager

    user = Fabricate(:user)
    timezone = Fabricate(:timezone, user: user)

    patch api_timezone_url(timezone), params: { "timezone" => {"name" => "Sweet Home", "difference" => '2'} },
                            headers: header_for(:manager)


    timezone.reload
    assert_equal "Amsterdam", timezone.city
    assert_equal "Amsterdam", timezone.name
    assert_equal 1.0, timezone.difference

    assert_response :success
  end

  # destroy
  def test_destroy_success
    user = Fabricate(:user)
    timezone = Fabricate(:timezone, user: user)

    delete api_timezone_url(timezone),
          headers: header_for(:admin)

    assert_equal 0, Timezone.count
    assert_response :success
  end


  def test_destroy_fail
    user = Fabricate(:user)
    timezone = Fabricate(:timezone, user: user)

    delete api_timezone_url(timezone),
          headers: header_for(:manager)

    assert_equal 1, Timezone.count
    assert_response :success
  end

  # search
  def test_timezone_successfull_search_by_user
    user = Fabricate(:user)
    timezone = Fabricate(:timezone, name: 'First', user: user)
    timezone = Fabricate(:timezone, name: 'Second', user: user)

    get search_api_timezones_url(), params: {keyword: 'First'},
          headers: header_for(:user, user: user)

    assert_equal 1, json_response.count
    assert_equal 'First', json_response.first["name"]
    assert_response :success
  end

  # search
  def test_timezone_successfull_search_by_admin
    user = Fabricate(:user)
    timezone = Fabricate(:timezone, name: 'First', user: user)
    timezone = Fabricate(:timezone, name: 'Second', user: user)

    get search_api_timezones_url(), params: {keyword: 'First'},
          headers: header_for(:admin)

    assert_equal 1, json_response.count
    assert_equal 'First', json_response.first["name"]
    assert_response :success
  end

  # search
  def test_timezone_failed_search_by_manager
    user = Fabricate(:user)
    timezone = Fabricate(:timezone, name: 'First', user: user)
    timezone = Fabricate(:timezone, name: 'Second', user: user)

    get search_api_timezones_url(), params: {keyword: 'First'},
          headers: header_for(:manager)

    assert_equal 0, json_response.count
    assert_response :success
  end

end
