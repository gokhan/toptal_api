require 'test_helper'

class AuthorizationTest < ActionDispatch::IntegrationTest
  include TokenHelpers

  def test_auth_token
    [:user, :manager, :admin].each do |role|
      get api_timezones_url, headers: header_for(role)

      assert_response :success
    end
  end

  def test_no_auth_token
    [:user, :manager, :admin].each do |role|
      get api_timezones_url

      assert_response :unauthorized
    end
  end

end
