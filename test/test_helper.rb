ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"
require "support/token_helpers"

# Uncomment for awesome colorful output
require "minitest/pride"

class ActiveSupport::TestCase
end
