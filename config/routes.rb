Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resources :users do
      get :me, on: :collection
    end
    resources :timezones do
      get :search, on: :collection
    end
  end

  post 'api/authenticate' => 'user_token#create', defaults: { format: :json } 
end
